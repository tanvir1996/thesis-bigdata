\begin{thebibliography}{10}

\bibitem{priyadarshini2018decision}
Ms~G Priyadarshini.
\newblock Decision tree algorithms for diagnosis of cardiac disease treatment.
\newblock 2018.

\bibitem{sultana2018sophisticated}
Sk~Sajida Sultana and Vasumathi~Devi Majety.
\newblock Sophisticated decision tree based id3 for analysing big data.
\newblock 2018.

\bibitem{lango2018multi}
Mateusz Lango and Jerzy Stefanowski.
\newblock Multi-class and feature selection extensions of roughly balanced
  bagging for imbalanced data.
\newblock {\em Journal of Intelligent Information Systems}, 50(1):97--127,
  2018.

\bibitem{abdallah2018fault}
I~Abdallah, V~Dertimanis, H~Mylonas, K~Tatsis, E~Chatzi, N~Dervilis, K~Worden,
  and E~Maguire.
\newblock Fault diagnosis of wind turbine structures using decision tree
  learning algorithms with big data.
\newblock In {\em Safety and Reliability--Safe Societies in a Changing World},
  pages 3053--3061. CRC Press, 2018.

\bibitem{jakhar2018predication}
Yogesh~Kumar Jakhar, Nidhi Mishra, and Rakesh Poonia.
\newblock Predication accuracy analysis of data mining algorithms on
  meteorological data using r programming.
\newblock 2018.

\bibitem{diaz2006gene}
Ram{\'o}n D{\'\i}az-Uriarte and Sara~Alvarez De~Andres.
\newblock Gene selection and classification of microarray data using random
  forest.
\newblock {\em BMC bioinformatics}, 7(1):3, 2006.

\bibitem{thiebault2018m2b}
Andr{\'e}a Thiebault, Laurent Dubroca, Ralf~HE Mullers, Yann Tremblay, and
  Pierre~A Pistorius.
\newblock m2b package in r: Deriving multiple variables from movement data
  to predict behavioural states with random forests.
\newblock {\em Methods in Ecology and Evolution}, 9(6):1548--1555, 2018.

\bibitem{hamby2008prediction}
Stephen~E Hamby and Jonathan~D Hirst.
\newblock Prediction of glycosylation sites using random forests.
\newblock {\em BMC bioinformatics}, 9(1):500, 2008.

\bibitem{khalilia2011predicting}
Mohammed Khalilia, Sounak Chakraborty, and Mihail Popescu.
\newblock Predicting disease risks from highly imbalanced data using random
  forest.
\newblock {\em BMC medical informatics and decision making}, 11(1):51, 2011.

\bibitem{basha2018impact}
Syed~Muzamil Basha, Dharmendra~Singh Rajput, and Vishnu Vandhan.
\newblock Impact of gradient ascent and boosting algorithm in classification.
\newblock {\em International Journal of Intelligent Engineering and Systems
  (IJIES)}, 11(1):41--49, 2018.

\bibitem{ceylan2018feature}
Rahime Ceylan and Mucahid Barstugan.
\newblock Feature selection using ffs and pca in biomedical data classification
  with adaboost-svm.
\newblock {\em International Journal of Intelligent Systems and Applications in
  Engineering}, 6(1):33--39, 2018.

\bibitem{zeng2015novel}
Zilin Zeng, Hongjun Zhang, Rui Zhang, and Chengxiang Yin.
\newblock A novel feature selection method considering feature interaction.
\newblock {\em Pattern Recognition}, 48(8):2656--2666, 2015.

\bibitem{maji2011rough}
Pradipta Maji and Sushmita Paul.
\newblock Rough set based maximum relevance-maximum significance criterion and
  gene selection from microarray data.
\newblock {\em International Journal of Approximate Reasoning}, 52(3):408--426,
  2011.

\bibitem{jiang2015relative}
Feng Jiang, Yuefei Sui, and Lin Zhou.
\newblock A relative decision entropy-based feature selection approach.
\newblock {\em Pattern Recognition}, 48(7):2151--2163, 2015.

\bibitem{sebban2002hybrid}
Marc Sebban and Richard Nock.
\newblock A hybrid filter/wrapper approach of feature selection using
  information theory.
\newblock {\em Pattern Recognition}, 35(4):835--846, 2002.

\bibitem{yan2011correntropy}
Hui Yan, Xiaotong Yuan, Shuicheng Yan, and Jingyu Yang.
\newblock Correntropy based feature selection using binary projection.
\newblock {\em Pattern Recognition}, 44(12):2834--2842, 2011.

\bibitem{hartigan1972direct}
John~A Hartigan.
\newblock Direct clustering of a data matrix.
\newblock {\em Journal of the american statistical association},
  67(337):123--129, 1972.

\end{thebibliography}
