\contentsline {section}{\numberline {1}Introduction}{10}
\contentsline {subsection}{\numberline {1.1}Motivation}{10}
\contentsline {subsection}{\numberline {1.2}Objectives of the Thesis}{10}
\contentsline {subsection}{\numberline {1.3}Organization of the Thesis}{11}
\contentsline {section}{\numberline {2}Related Work}{11}
\contentsline {subsection}{\numberline {2.1}Co-relation base feature selection with Clustering}{11}
\contentsline {subsubsection}{\numberline {2.1.1}Decision Tree}{11}
\contentsline {subsubsection}{\numberline {2.1.2}Bagging Algorithm}{14}
\contentsline {subsubsection}{\numberline {2.1.3}Random Forest}{17}
\contentsline {subsubsection}{\numberline {2.1.4}AdaBoost Algorithm}{19}
\contentsline {subsubsection}{\numberline {2.1.5}Voting Algorithm(Svm,C4.5,Logistic Regression)}{21}
\contentsline {subsubsection}{\numberline {2.1.6}Logistic Regression}{21}
\contentsline {subsubsection}{\numberline {2.1.7}Decision tree (CART)}{23}
\contentsline {subsubsection}{\numberline {2.1.8}SVM algorithm}{23}
\contentsline {section}{\numberline {3}Proposed Method}{23}
\contentsline {subsection}{\numberline {3.1} Data Preprocessing}{23}
\contentsline {subsubsection}{\numberline {3.1.1} Missing Value Handling By k-NN Imputation}{25}
\contentsline {subsection}{\numberline {3.2}Co-relation Feature Grouping Method}{25}
\contentsline {subsection}{\numberline {3.3}Find Covariance Matrix for Each Sample}{25}
\contentsline {subsection}{\numberline {3.4}Clustering of Matrices}{26}
\contentsline {subsection}{\numberline {3.5}Random Features from Cluster Group}{26}
\contentsline {section}{\numberline {4}Data Set and Comparison of Performance}{27}
\contentsline {subsubsection}{\numberline {4.0.1}Decision Tree}{30}
\contentsline {subsubsection}{\numberline {4.0.2}Bagging Algorithm}{30}
\contentsline {subsubsection}{\numberline {4.0.3}Random Forest}{31}
\contentsline {subsubsection}{\numberline {4.0.4}AdaBoost Algorithm}{31}
\contentsline {subsubsection}{\numberline {4.0.5}Voting Algorithm(Svm,C4.5,Logistic Regression)}{32}
\contentsline {subsubsection}{\numberline {4.0.6}Logistic Regression}{32}
\contentsline {subsubsection}{\numberline {4.0.7}Decision tree (CART)}{33}
\contentsline {subsubsection}{\numberline {4.0.8}SVM algorithm}{33}
\contentsline {section}{\numberline {5}Conclusion}{34}
